import datetime as dt
import sys
import os
import logging
import subprojects.misc.LowLevelFuncions as llw
from subprojects.FileProcessor import FileProcessor
from subprojects.StatisticsManager import StatisticsManager

logger = llw.script_logger('POST-PROCESSOR')


class ProgramData:

    def __init__(self, program_name, program_version):
        self.program_name = program_name
        self.program_version = program_version
        self.reference_timestamp = str(dt.datetime.now().strftime("%y%m%d_%H%M%S"))

    def log_header_line(self):
        logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (self.program_name, self.program_version))

    def log_version_line_and_exit(self):
        print('%s (version: %s)' % (self.program_name, self.program_version))
        exit(0)

    def log_man_page_and_exit(self):
        print('##### PROGRAM %s ( VERSION: %s ) #####' % (self.program_name, self.program_version))
        print('Usage: python3 %s [compulsory inputs] {optional inputs}' % sys.argv[0])
        print('Usage: python3 %s [trade dir] {optional inputs}' % sys.argv[0].split('/')[-1])
        print()
        print('Allowed options:                                                                     \n'
              '                                                                                     \n'
              'General options:                                                                     \n' 
              '  --help                                     Produce help message                    \n'
              '  --version                                  Output the version                      \n'
              '                                                                                     \n')
        exit(0)

    def detect_standard_inputs(self, argv):

        if '--help' in argv:
            self.log_man_page_and_exit()
        elif '--version' in argv:
            self.log_version_line_and_exit()

    def get_compulsory_inputs(self):
        if len(sys.argv) < 2:
            logger.error('Wrong number of inputs received')
            program_data.log_man_page_and_exit()

        else:
            self.trade_path = sys.argv[1]

    def get_optional_inputs(self):
        pass

##################################################
#  INITIAL OPS  ##################################
##################################################


program_data = ProgramData('Post-Processor ', 'v0')

# INPUT COMPULSORY DATA ###
#####################

program_data.detect_standard_inputs(sys.argv)

program_data.get_compulsory_inputs()

try:
    os.stat(program_data.trade_path)
except:
    logger.error('No trading folder found. Program aborted')
    sys.exit()

logging.basicConfig(filename=program_data.trade_path + '/postprocessor.log', filemode='a',
                    format='%(asctime)s [%(levelname)-8s] [%(name)-17s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (program_data.program_name,
                                                                program_data.program_version))

# INPUT OPTIONAL DATA ###
###################

# logger.info('Processing optional input data')

##################################################
#  PROCESS LAUNCHING  ############################
##################################################

file_processor = FileProcessor(None, None, program_data.trade_path)
equity_data = file_processor.get_equity_data()
orders_data = file_processor.get_orders_data()

statistics_manager = StatisticsManager('', program_data.trade_path, equity_data, orders_data)

try:
    statistics_manager.generate_trading_plots()
except Exception as e:
    logger.error('Error generating trading plots')
    logger.error(e)

try:
    statistics_manager.generate_trading_report()
except Exception as e:
    logger.error('Error generating trading report')
    logger.error(e)

try:
    statistics_manager.generate_trade_statistics()
except Exception as e:
    logger.error('Error generating trade estatistics')
    logger.error(e)

logger.info('### PROGRAM FINISHED ###')
