import datetime as dt
import sys
import os
import logging
import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Common import *
from data_server.HistoricalDataServer import HistoricalDataServer
from data_server.RealtimeDataServer import RealtimeDataServer
from subprojects.resources.Constants import SymbolsResources
from subprojects.FileProcessor import FileProcessor

logger = llw.script_logger('DATA SERVER')


class ProgramData:

    def __init__(self, program_name, program_version):
        self.program_name = program_name
        self.program_version = program_version
        self.reference_timestamp = str(dt.datetime.now().strftime("%y%m%d_%H%M%S"))

    def log_header_line(self):
        logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (self.program_name, self.program_version))

    def log_version_line_and_exit(self):
        print('%s (version: %s)' % (self.program_name, self.program_version))
        exit(0)

    def log_man_page_and_exit(self):
        print('##### PROGRAM %s ( VERSION: %s ) #####' % (self.program_name, self.program_version))
        print('Usage: python3 %s [compulsory inputs] {optional inputs}' % sys.argv[0])
        print('Usage: python3 %s [config dir] [session id] [log dir] {optional inputs}' % sys.argv[0].split('/')[-1])
        print()
        print('Allowed options:                                                                     \n'
              '                                                                                     \n'
              'General options:                                                                     \n' 
              '  --help                                     Produce help message                    \n'
              '  --version                                  Output the version                      \n'
              '                                                                                     \n')
        exit(0)

    def detect_standard_inputs(self, argv):

        if '--help' in argv:
            self.log_man_page_and_exit()
        elif '--version' in argv:
            self.log_version_line_and_exit()

    def get_compulsory_inputs(self):
        if len(sys.argv) < 4:
            logger.error('Wrong number of inputs received')
            program_data.log_man_page_and_exit()

        else:
            self.config_path = sys.argv[1]
            self.session_id = sys.argv[2]
            self.log_path = sys.argv[3]

    def get_optional_inputs(self):
        pass

##################################################
#  INITIAL OPS  ##################################
##################################################


program_data = ProgramData('Data Server ', 'v0')

# INPUT COMPULSORY DATA ###
#####################

program_data.detect_standard_inputs(sys.argv)

program_data.get_compulsory_inputs()

try:
    os.stat(program_data.log_path)
except:
    os.mkdir(program_data.log_path)

# INPUT OPTIONAL DATA ###
###################

# logger.info('Processing optional input data')

##################################################
#  PROCESS LAUNCHING  ############################
##################################################

logging.basicConfig(filename=program_data.log_path + '/' + program_data.session_id + '.log', filemode='a',
                    format='%(asctime)s [%(levelname)-8s] [%(name)-17s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (program_data.program_name,
                                                                program_data.program_version))

file_processor = FileProcessor(program_data.config_path, program_data.session_id, program_data.log_path)
logger.info('Processing main configuration')
config = file_processor.get_data_server_config()

# Load symbols

if config.general.market == Market.MC:
    symbol_list = SymbolsResources.MC_SYMBOLS
elif config.general.market == Market.DE:
    symbol_list = SymbolsResources.DE_SYMBOLS
elif config.general.market == Market.FX:
    symbol_list = SymbolsResources.FX_SYMBOLS
elif config.general.market == Market.CC:
    symbol_list = SymbolsResources.CC_SYMBOLS
elif config.general.market == Market.US:
    symbol_list = SymbolsResources.US_SYMBOLS

logger.info('Mode: %s', config.general.mode.value)

if config.general.mode == DataServerMode.HISTORICAL:
    data_server = HistoricalDataServer()
    data_server.run_historical_data_server(config, program_data.session_id, symbol_list)

if config.general.mode == DataServerMode.REALTIME:
    data_server = RealtimeDataServer(config, program_data.session_id, symbol_list)
    data_server.run_realtime_data_server()

logger.info('### PROGRAM FINISHED ###')
