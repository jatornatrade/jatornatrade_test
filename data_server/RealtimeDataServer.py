import datetime as dt
import subprojects.misc.LowLevelFuncions as llw
from subprojects.misc.LowLevelFuncions import internet_connection
from subprojects.types.Common import *
from data_server.DataHandler import DataHandler
from subprojects.misc.LowLevelFuncions import next_monday_9am
from subprojects.resources.Constants import MarketCalendar
from subprojects.CommManager import CommManager
from data_server.StreamingDataManager import StreamingDataManager
import MySQLdb as mdb
import pandas as pd
import time

logger = llw.script_logger('REALTIME DS')


class RealtimeDataServer:

    def __init__(self, config, session_id, symbol_list):
        self.config = config
        self.session_id = session_id
        self.symbol_list = symbol_list
        self.symbol_data = {}

    def load_today_symbol_data(self, data_sql, symbol):
        bars = pd.DataFrame(columns=['date', 'open', 'low', 'high', 'close', 'volume'])
        current_date = pd.Timestamp(dt.date.today().year, dt.date.today().month, dt.date.today().day)
        data_sql = data_sql[data_sql['date'] > current_date]
        bars['date'] = data_sql[data_sql['symbol'] == symbol]['date'].values
        bars['open'] = data_sql[data_sql['symbol'] == symbol]['open'].values
        bars['low'] = data_sql[data_sql['symbol'] == symbol]['low'].values
        bars['high'] = data_sql[data_sql['symbol'] == symbol]['high'].values
        bars['close'] = data_sql[data_sql['symbol'] == symbol]['close'].values
        bars['volume'] = data_sql[data_sql['symbol'] == symbol]['volume'].values
        bars.set_index('date')
        return bars

    def run_realtime_data_server(self):

        try:
            logger.info('Launching realtime data server')
            logger.info('Session ID: '+self.session_id)
            logger.info('Database path: ' + self.config.general.db_path)
            logger.info('Market: ' + self.config.general.market.value)
            logger.info('Source: ' + self.config.general.source.value)
            logger.info('Server Address: ' + self.config.realtime.hostname)
            logger.info('Server Port: ' + str(self.config.realtime.port))

            # Initializate objects and variables

            open_market = MarketCalendar.MARKET_SCHEDULE[self.config.general.market.value]['Open']
            close_market = MarketCalendar.MARKET_SCHEDULE[self.config.general.market.value]['Close']

            comm_mgr = CommManager(self.config.realtime)

            comm_mgr.listen_incoming_connections()

            logger.info('Loading current data')
            # Create mysql connection
            mysql_connection = mdb.connect(self.config.mysqlconf.db_host, self.config.mysqlconf.db_user,
                                           self.config.mysqlconf.db_pass, self.config.mysqlconf.db_name)
            data_sql = DataHandler.get_today_market_data(self.config.general.market, mysql_connection)

            for symbol in self.symbol_list:
                self.symbol_data[symbol] = self.load_today_symbol_data(data_sql, symbol)
            del data_sql

            # Create parser streaming manager
            stream_data_mgr = StreamingDataManager(self.config.general.chromedriver_path, self.config.general.source,
                                                   mysql_connection)
            logger.info('Starting realtime data server')

            is_24_hours_market = self.config.general.market == Market('CC') or \
                                 self.config.general.market == Market('FX')

            while True:

                if internet_connection():
                    if not is_24_hours_market:
                        week_day = dt.datetime.now().weekday()
                        if week_day == 5 or week_day == 6 or dt.datetime.now().time() > close_market:
                            logger.info('Market closed')
                            if week_day == 4 or week_day == 5 or week_day == 6:
                                next_stream_date = next_monday_9am()
                            else:
                                next_stream_date = dt.datetime(dt.datetime.now().year, dt.datetime.now().month,
                                                               dt.datetime.now().day,
                                                               open_market.hour, open_market.minute,
                                                               open_market.second) + dt.timedelta(days=1)

                            logger.info('Streaming will start at: ' + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))
                            time.sleep((next_stream_date - dt.datetime.now()).total_seconds() - stream_data_mgr.login_time)

                            # Reload symbol data
                            logger.info('Loading current data')
                            mysql_connection = mdb.connect(self.config.mysqlconf.db_host, self.config.mysqlconf.db_user,
                                                           self.config.mysqlconf.db_pass, self.config.mysqlconf.db_name)
                            data_sql = DataHandler.get_today_market_data(self.config.general.market, mysql_connection)
                            for symbol in self.symbol_list:
                                self.symbol_data[symbol] = self.load_today_symbol_data(data_sql, symbol)
                            del data_sql

                        elif dt.datetime.now().time() < open_market:
                            logger.info('Market closed')
                            next_stream_date = dt.datetime(dt.datetime.now().year, dt.datetime.now().month,
                                                           dt.datetime.now().day, open_market.hour, open_market.minute,
                                                           open_market.second)
                            logger.info('Streaming will start at: ' + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))
                            temp = next_stream_date - dt.timedelta(seconds=stream_data_mgr.login_time)
                            if dt.datetime.now().time() < temp.time():
                                time.sleep((next_stream_date - dt.datetime.now()).total_seconds() -
                                           stream_data_mgr.login_time)

                            # Reload symbol data
                            logger.info('Loading current data')
                            mysql_connection = mdb.connect(self.config.mysqlconf.db_host, self.config.mysqlconf.db_user,
                                                           self.config.mysqlconf.db_pass, self.config.mysqlconf.db_name)
                            data_sql = DataHandler.get_today_market_data(self.config.general.market, mysql_connection)
                            for symbol in self.symbol_list:
                                self.symbol_data[symbol] = self.load_today_symbol_data(data_sql, symbol)
                            del data_sql

                        else:
                            next_stream_date = dt.datetime(dt.datetime.now().year, dt.datetime.now().month,
                                                           dt.datetime.now().day, dt.datetime.now().hour,
                                                           dt.datetime.now().minute) + dt.timedelta(seconds=60)
                            logger.info('Streaming will start at: ' + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))
                    else:
                        next_stream_date = dt.datetime(dt.datetime.now().year, dt.datetime.now().month,
                                                       dt.datetime.now().day, dt.datetime.now().hour,
                                                       dt.datetime.now().minute) + dt.timedelta(seconds=60)
                        logger.info('Streaming will start at: ' + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))

                    stream_data_mgr.login()
                    if not stream_data_mgr.logged:
                        logger.info('Restarting connection')
                        continue

                    if (dt.datetime.now() + dt.timedelta(seconds=stream_data_mgr.parse_time)) > next_stream_date:
                        datetime_now_latency = dt.datetime.now() + dt.timedelta(seconds=stream_data_mgr.parse_time)
                        next_stream_date = dt.datetime(datetime_now_latency.year, datetime_now_latency.month,
                                                       datetime_now_latency.day, datetime_now_latency.hour,
                                                       datetime_now_latency.minute) + dt.timedelta(seconds=60)
                        logger.warning('Latency detected. New stream data scheduled: '
                                       + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))
                    try:
                        time.sleep((next_stream_date - dt.datetime.now()).total_seconds() -
                                   dt.datetime.now().microsecond / 1000000 - stream_data_mgr.parse_time)
                    except ValueError:
                        continue

                    data_server_started = True
                    get_streaming_data_attempts = 0
                    while True:
                        if not internet_connection():
                            break

                        next_stream_date += dt.timedelta(seconds=self.config.realtime.rate)
                        if dt.datetime.now().time() > close_market and not is_24_hours_market:
                            if stream_data_mgr.source != DataSource.REPLAY:
                                stream_data_mgr.driver.quit()
                            break

                        if not data_server_started:
                            logger.info('Streaming data started')
                            data_server_started = True

                        tick_dict = stream_data_mgr.get_streaming_data(self.config.general.market, self.symbol_data,
                                                                       mysql_connection)
                        # TODO: Implement other fail check
                        if len(tick_dict) > 0:
                            logger.info('New symbols streamed: ' + str(len(tick_dict)))
                            get_streaming_data_attempts = 0
                            logger.log(5, 'Parse data market completed correctly')
                        else:
                            logger.warning('No new ticks')
                            # if not stream_data_mgr.last_tick_status:
                            get_streaming_data_attempts += 1
                            if get_streaming_data_attempts == 2:
                                logger.error('Too many attempts with empty streaming data. Restarting connection')
                                if stream_data_mgr.source != DataSource.REPLAY:
                                    stream_data_mgr.driver.quit()
                                break
                            logger.warning('Parse data market could be an error ')

                        if (dt.datetime.now() + dt.timedelta(seconds=stream_data_mgr.parse_time)) > next_stream_date:
                            datetime_now_latency = dt.datetime.now() + dt.timedelta(seconds=stream_data_mgr.parse_time)
                            next_stream_date = dt.datetime(datetime_now_latency.year, datetime_now_latency.month,
                                                           datetime_now_latency.day, datetime_now_latency.hour,
                                                           datetime_now_latency.minute) + dt.timedelta(seconds=60)
                            logger.warning('Latency detected. New stream data scheduled: '
                                           + next_stream_date.strftime('%Y-%m-%d %H:%M:%S'))
                            try:
                                time.sleep((next_stream_date - dt.datetime.now()).total_seconds() -
                                           dt.datetime.now().microsecond / 1000000 - stream_data_mgr.parse_time)
                            except ValueError:
                                continue
                            continue

                        # if len(tick_dict) > 0:

                        comm_mgr.accept_incoming_connections()
                        comm_mgr.send_market_data_msg(self.symbol_data)

                        try:
                            time.sleep((next_stream_date - dt.datetime.now()).total_seconds() -
                                       dt.datetime.now().microsecond / 1000000 - stream_data_mgr.parse_time)
                        except ValueError:
                            pass

                else:
                    logger.error('No internet connection')
                    logger.info('Retrying in 30 seconds')
                    if stream_data_mgr.source != DataSource.REPLAY:
                        stream_data_mgr.driver.quit()
                    time.sleep(30)

        finally:
            if stream_data_mgr.source != DataSource.REPLAY:
                stream_data_mgr.driver.quit()
