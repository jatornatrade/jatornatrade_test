from pandas_datareader import data as pdr
from alpha_vantage.timeseries import TimeSeries
from fhub import Session
import datetime as dt
import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Common import *
import sys

logger = llw.script_logger('HIST DATA WRAPPER')


class HistoricalDataWrapper:

    def __init__(self, alpha_key, fhub_key):
        self.ts = TimeSeries(alpha_key, output_format='pandas')
        self.hub = Session(fhub_key)
        self.tomorrow_str = (dt.datetime.today() + dt.timedelta(days=1)).strftime(format='%Y-%m-%d')
        self.today_str = (dt.datetime.today()).strftime(format='%Y-%m-%d')

    def download_from_pdr(self, symbol, frequency, market):

        if market == Market.FX:
            symbol = symbol + '=X'

        if frequency == Frequency.DAILY:

            data = pdr.get_data_yahoo(symbol, start='01-01-2000', end=self.tomorrow_str)
            data.index = data.index.normalize()
            data.index = data.index.tz_localize(None)
            data.index.names = ['date']
            adjust_factor = (data['Adj Close'] / data['Close']).values
            data['Close'] = data['Adj Close']
            del data['Adj Close']
            data.columns = ['high', 'low', 'open', 'close', 'volume']
            data = data.sort_values(['date'], ascending=[True])
            data = data[['open', 'high', 'low', 'close', 'volume']]
            data['open'] = data['open'] * adjust_factor
            data['high'] = data['high'] * adjust_factor
            data['low'] = data['low'] * adjust_factor
            if market != Market.FX:
                data = data[data['volume'] > 0]
            data = data.sort_values(['date'], ascending=[True])

            return data
        else:
            logger.error('No intraday data source implement for yahoo')
            logger.info('Program aborted')
            sys.exit()

    def download_from_fhub(self, symbol, frequency, market):

        if market == Market.FX:
            symbol = symbol + '=X'

        if frequency == Frequency.DAILY:

            data = self.hub.candle(symbol, end=self.tomorrow_str)
            data.index = data.index.normalize()
            data.index = data.index.tz_localize(None)
            data.index.names = ['date']
            data = data[['open', 'high', 'low', 'close', 'volume']]
            if market != Market.FX:
                data = data[data['volume'] > 0]
            data = data.sort_values(['date'], ascending=[True])

            return data

        if frequency == Frequency.INTRADAY:

            resolution = 60

            kind = 'stock'

            if market == Market.CC:
                symbol = symbol.replace('-', '')
                symbol = 'BINANCE:' + symbol + 'T'
                kind = 'crypto'
                start_date = ((dt.datetime.today() - dt.timedelta(minutes=450*resolution)).strftime(format='%Y-%m-%d'))

            data = self.hub.candle(symbol, kind=kind, start=start_date, end=self.today_str, resolution=resolution)
            data.index = data.index.tz_localize(None)
            data.index = data.index.tz_localize('US/Eastern', True).tz_convert('Europe/Madrid')
            data.index = data.index.tz_localize(None)
            data.index.names = ['date']
            data = data[['open', 'high', 'low', 'close', 'volume']]
            if market != Market.FX:
                data = data[data['volume'] > 0]

            data = data.sort_values(['date'], ascending=[True])

            return data

    def download_from_alpha(self, symbol, frequency, market):

        if frequency == Frequency.INTRADAY:

            data = self.ts.get_intraday(symbol=symbol, interval='1min', outputsize='full')[0]
            data.columns = ["open", "high", "low", "close", "volume"]
            # Convert timezone
            data.index = data.index.tz_localize('US/Eastern').tz_convert('Europe/Madrid')
            data.index = data.index.tz_localize(None)
            data.index.names = ['date']
            if market != Market.FX:
                data = data[data['volume'] > 0]
            data = data.sort_values(['date'], ascending=[True])

            return data

        if frequency == Frequency.DAILY:
            logger.error('No daily data source implement for alpha')
            logger.info('Program aborted')
            sys.exit()
