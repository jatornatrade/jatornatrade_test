import os
import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Common import *
import pandas as pd
import time
import MySQLdb as mdb
from subprojects.AlertManager import AlertManager
from data_server.DataHandler import DataHandler
from data_server.HistoricalDataWrapper import HistoricalDataWrapper

logger = llw.script_logger('HISTORICAL DS')


class HistoricalDataServer:

    def run_historical_data_server(self, config, session_id, symbol_list):

        logger.info('Launching historical data server')
        logger.info('Session ID: ' + session_id)
        database_path = config.general.db_path
        logger.info('Database path; ' + database_path)
        frequency = config.historical.frequency
        logger.info('Data source: ' + config.general.source.value)
        logger.info('Frequency: ' + frequency.value)
        # TODO: Implement Key manage
        alpha_key = config.historical.alpha_key
        fhub_key = config.historical.fhub_key
        logger.info('Market: ' + config.general.market.value)
        logger.info('Telegram Alerts: ' + str(config.general.tgm_alerts))

        market = config.general.market
        try:
            os.stat(database_path + '/' + 'historical' + '/' + frequency.value.lower() + '/' + market.value)
        except:
            os.mkdir(database_path + '/' + 'historical' + '/' + frequency.value.lower() + '/' + market.value)

        symbol_list = symbol_list
        logger.info('Symbols tracked: ' + str(symbol_list))

        logger.info('Historical data download started')

        symbol_error = []
        i = 0

        data_wrapper = HistoricalDataWrapper(alpha_key, fhub_key)
        alert_mgr = AlertManager(config, True)

        for symbol in symbol_list:
            try:
                symbol_file = symbol.replace('.', '_')

                if config.general.source == DataSource.YAHOO:
                    data = data_wrapper.download_from_pdr(symbol, frequency, market)
                elif config.general.source == DataSource.FINNHUB:
                    data = data_wrapper.download_from_fhub(symbol, frequency, market)
                elif config.general.source == DataSource.ALPHA:
                    data = data_wrapper.download_from_alpha(symbol, frequency, market)

                if len(data) == 0:
                    logger.error('Error with symbol ' + symbol)
                    symbol_error.append(symbol)
                    continue

                try:
                    os.stat(database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv')
                    if frequency == Frequency.DAILY:
                        data.to_csv(
                            database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                            sep='\t', float_format='%.4f', mode='w', header=True, date_format='%Y-%m-%d')
                    else:
                        data.to_csv(
                            database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                            sep='\t', float_format='%.4f', mode='a', header=False)

                    logger.log(5, symbol + ' csv file saved')

                except:
                    if frequency == Frequency.DAILY:
                        data.to_csv(
                            database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                            sep='\t', float_format='%.4f', mode='w', header=True, date_format='%Y-%m-%d')

                    else:
                        data.to_csv(
                            database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                            sep='\t', float_format='%.4f', mode='a', header=True)
                    logger.log(5, symbol + ' csv file saved')

            except Exception as e:
                logger.error('Error with symbol ' + symbol)
                logger.error(e)
                symbol_error.append(symbol)

            i += 1
            if i == 5:
                i = 0
                if frequency == Frequency.INTRADAY:
                    time.sleep(70)

        attempt = 1
        symbol_error_tmp = symbol_error.copy()
        while len(symbol_error) != 0 and attempt < 6:
            logger.info('Retrying errors data: ')
            logger.info(symbol_error)
            for symbol in symbol_error:
                try:
                    symbol_file = symbol.replace('.', '_')
                    if config.historical.source == DataSource.YAHOO:
                        data = data_wrapper.download_from_pdr(symbol, frequency, market)
                    elif config.historical.source == DataSource.FINNHUB:
                        data = data_wrapper.download_from_fhub(symbol, frequency, market)
                    elif config.historical.source == DataSource.ALPHA:
                        data = data_wrapper.download_from_alpha(symbol, frequency, market)

                    if len(data) == 0:
                        logger.error('Error with symbol ' + symbol)
                        continue

                    try:
                        os.stat(database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv')
                        if frequency == Frequency.DAILY:
                            data.to_csv(
                                database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                                sep='\t', float_format='%.4f', mode='w', header=True, date_format='%Y-%m-%d')
                        else:
                            data.to_csv(
                                database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                                sep='\t', float_format='%.4f', mode='a', header=False)

                        logger.log(5, symbol + ' csv file saved')
                        symbol_error_tmp.remove(symbol)

                    except:
                        if frequency == Frequency.DAILY:
                            data.to_csv(
                                database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                                sep='\t', float_format='%.4f', mode='w', header=True, date_format='%Y-%m-%d')

                        else:
                            data.to_csv(
                                database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file + '.csv',
                                sep='\t', float_format='%.4f', mode='a', header=True)
                        logger.log(5, symbol + ' csv file saved')
                        symbol_error_tmp.remove(symbol)

                except Exception as e:
                    logger.error('Error with symbol ' + symbol)

                i += 1
                if i == 5:
                    i = 0
                    if frequency == Frequency.INTRADAY:
                        time.sleep(70)
            attempt += 1
            symbol_error = symbol_error_tmp.copy()

        logger.info('Download data finished')

        # Mysql connection
        con = mdb.connect(config.mysqlconf.db_host, config.mysqlconf.db_user, config.mysqlconf.db_pass,
                          config.mysqlconf.db_name)

        for symbol in symbol_list:
            if symbol in symbol_error:
                logger.error('No new data for ' + symbol + ' . Cleaning and sql storing discarted')
                continue

            symbol_file = symbol.replace('.', '_')

            try:
                data = pd.read_csv(
                    database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' + symbol_file +
                    '.csv', delimiter='\t')
            except:
                logger.error('Not found ' + symbol + ' data')
                continue

            data = data.drop_duplicates('date', keep='last')
            data = data.sort_values('date')
            data.columns = ['date', 'open', 'high', 'low', 'close', 'volume']
            data.to_csv(database_path + '/historical/' + frequency.value.lower() + '/' + market.value + '/' +
                        symbol_file + '.csv', sep='\t', float_format='%.4f', index=False)

            logger.log(5, symbol_file + ' file cleaned')

            # Insert data to mysql database

            try:
                data_sql = pd.DataFrame()
                data_sql['date'] = data['date'].values
                data_sql['symbol'] = symbol
                data_sql['exchange'] = market
                data_sql['open'] = data['open'].values
                data_sql['high'] = data['high'].values
                data_sql['low'] = data['low'].values
                data_sql['volume'] = data['volume'].values
                data_sql['close'] = data['close'].values

                if frequency == Frequency.INTRADAY:
                    data_sql['source'] = config.general.source

                DataHandler.insert_hist_data_db(frequency, data_sql, market, con)

                logger.log(5, symbol + ' sql saved')

            except Exception as e:
                logger.error(e)
                logger.error('Error storing symbol ' + symbol + ' in '+ config.mysqlconf.db_name)

        logger.info('Cleaning data finished')

        msg = 'Data Server info\n'
        msg += 'Session: ' + session_id + \
               '\nMarket: ' + market.value + \
               '\nFrequency: ' + frequency.value + \
               '\nDownload ' + str(len(symbol_list) - len(symbol_error)) + ' out of ' + str(len(symbol_list)) + \
               ' symbols'
        if len(symbol_error) > 0:
            msg += '\nSymbols error: ' + str(symbol_error)

        alert_mgr.send_telegram_alert(msg)
        logger.info(msg)
