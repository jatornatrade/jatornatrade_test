import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Common import *
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from data_server.data_market_wrappers.bolsamania_wrapper import login_bolsamania
from data_server.data_market_wrappers.bolsamania_wrapper import get_bolsamania_ticks
from data_server.data_market_wrappers.replay_wrapper import get_replay_ticks
import time
import datetime as dt
from subprojects.types.Tick import Tick
from data_server.DataHandler import DataHandler

logger = llw.script_logger('STREAM DATA MGR')

'''
Global options for chromedriver
'''

options_ = Options()
options_.add_argument('--headless')
options_.add_argument('--disable-gpu')
options_.add_argument("start-maximized")
options_.add_argument("disable-infobars")
options_.add_argument("--disable-extensions")
options_.add_argument("window-size=1920x1080")
options_.add_argument('--blink-settings=imagesEnabled=false')


class StreamingDataManager:

    def __init__(self, chromedriver_path, source, con):
        self.chromedriver_path = chromedriver_path
        self.source = source
        self.logged = False

        if self.source == DataSource.BOLSAMANIA:
            self.login_time = 180
            self.parse_time = 10
        elif self.source == DataSource.REPLAY:
            self.login_time = 0
            self.parse_time = 0

        self.tick_dict_old = {}
        self.last_tick_status = True
        self.driver = ''
        self.mysql_con = con

    def login(self):

        if self.source == DataSource.BOLSAMANIA:
            self.driver = webdriver.Chrome(self.chromedriver_path, options=options_)
            self.driver.delete_all_cookies()
            self.logged = login_bolsamania(self.driver)

        if self.source == DataSource.REPLAY:
            time.sleep(1)
            self.logged = True

        if self.logged:
            logger.log(5, 'Login performed succesfully')
        else:
            logger.error('Error with source login')
            self.driver.quit()

    def get_streaming_data(self, market, symbol_data, con):
        self.last_tick_status = True
        for symbol in symbol_data:
            self.tick_dict_old[symbol] = Tick()
            self.tick_dict_old[symbol].symbol = symbol
            if len(symbol_data[symbol]) > 0:
                self.tick_dict_old[symbol].volume = symbol_data[symbol]['volume'].values[-1]
                self.tick_dict_old[symbol].price = symbol_data[symbol]['close'].values[-1]

        try:
            if self.source == DataSource.BOLSAMANIA:
                tick_dict = get_bolsamania_ticks(self.driver, symbol_data)
            elif self.source == DataSource.REPLAY:
                tick_dict = get_replay_ticks()

            symbol_list = list(tick_dict.keys())

        except Exception as e:
            logger.error('Error parsing data')
            logger.error(str(e))
            symbol_list = []
            self.last_tick_status = False
            return symbol_list

        if len(symbol_list) == 0:
            logger.error('Error parsing data')
            self.last_tick_status = False
            return symbol_list

        for tick in symbol_list:
            try:
                # TODO: watch out! Implement real equal check
                if tick_dict[tick].volume <= self.tick_dict_old[tick].volume and market != Market('CC') and \
                        market != Market.FX:
                    del tick_dict[tick]
            except:
                pass

        for symbol in tick_dict:
            current_date = dt.datetime(dt.datetime.now().year, dt.datetime.now().month, dt.datetime.now().day,
                                       dt.datetime.now().hour, dt.datetime.now().minute)
            symbol_data[symbol].loc[len(symbol_data[symbol])] = [current_date, tick_dict[symbol].price,
                                                                 tick_dict[symbol].price, tick_dict[symbol].price,
                                                                 tick_dict[symbol].price, tick_dict[symbol].volume]

            if self.source != DataSource.REPLAY:
                try:
                    data_sql = [symbol_data[symbol]['date'].values[-1], symbol, market,
                                symbol_data[symbol]['close'].values[-1], symbol_data[symbol]['close'].values[-1],
                                symbol_data[symbol]['close'].values[-1], symbol_data[symbol]['close'].values[-1],
                                symbol_data[symbol]['volume'].values[-1], self.source]

                    DataHandler.insert_rt_data_db(data_sql, con)

                except Exception as e:
                    logger.error(e)
                    logger.error('Error storing symbol ' + symbol + ' in securities master')

        return tick_dict







