import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Common import *
import pandas as pd

logger = llw.script_logger('DATA HANDLER')


class DataHandler:

    @staticmethod
    def insert_hist_data_db(frequency, symbol_data, market, con):

        if frequency == Frequency.INTRADAY:
            table = 'intraday_price'
            column_str = "date, symbol, exchange, open, high, low, close, volume, source"
            data = []
            for i in range(len(symbol_data)):
                mean_close = (symbol_data['open'].values[i] + symbol_data['high'].values[i] + symbol_data['low'].values[
                    i]) / 3
                if symbol_data['close'].values[i] > 5 * mean_close or symbol_data['close'].values[i] <= 0 or \
                        symbol_data['volume'].values[i] < 0:
                    continue
                row_data = (symbol_data['date'].values[i], symbol_data['symbol'].values[i], market.value,
                            symbol_data['open'].values[i], symbol_data['high'].values[i], symbol_data['low'].values[i],
                            symbol_data['close'].values[i], symbol_data['volume'].values[i],
                            symbol_data['source'].values[i].value)
                data.append(row_data)
                insert_str = ("%s, " * 9)[:-2]

        if frequency == Frequency.DAILY:
            table = 'daily_price'
            column_str = "date, symbol, exchange, open, high, low, close, volume"
            data = []
            for i in range(len(symbol_data)):
                mean_close = (symbol_data['open'].values[i] + symbol_data['high'].values[i] + symbol_data['low'].values[
                    i]) / 3
                if symbol_data['close'].values[i] > 5 * mean_close or symbol_data['close'].values[i] <= 0 or \
                        symbol_data['volume'].values[i] < 0:
                    continue
                row_data = (symbol_data['date'].values[i], symbol_data['symbol'].values[i], market.value,
                            symbol_data['open'].values[i], symbol_data['high'].values[i],
                            symbol_data['low'].values[i], symbol_data['close'].values[i],
                            symbol_data['volume'].values[i])
                data.append(row_data)
                insert_str = ("%s, " * 8)[:-2]

        final_str = ("INSERT INTO %s (%s) VALUES (%s)" % (table, column_str, insert_str))
        # Using the MySQL connection, carry out an INSERT INTO for every symbol
        cur = con.cursor()
        insert_stmt = ("DELETE FROM %s WHERE (symbol = '%s');" % (table, symbol_data['symbol'].values[0]))
        cur.execute(insert_stmt)
        cur.executemany(final_str, data)
        con.commit()

    @staticmethod
    def insert_rt_data_db(symbol_data, con):

        table = 'intraday_price'
        column_str = "date, symbol, exchange, open, high, low, close, volume, source"
        data = []
        mean_close = (symbol_data[3] + symbol_data[4] + symbol_data[5])/3

        # TODO Check this
        if symbol_data[6] > (5 * mean_close) or symbol_data[6] <= 0 or symbol_data[6] <= 0:
            return

        row_data = (symbol_data[0], symbol_data[1], symbol_data[2].value, symbol_data[3], symbol_data[4], symbol_data[5],
                    symbol_data[6], symbol_data[7], symbol_data[8].value)
        data.append(row_data)

        insert_str = ("%s, " * 9)[:-2]
        final_str = ("INSERT INTO %s (%s) VALUES (%s)" % (table, column_str, insert_str))
        # Using the MySQL connection, carry out an INSERT INTO for every symbol
        cur = con.cursor()
        cur.executemany(final_str, data)
        con.commit()

    @staticmethod
    def get_market_data(market, con):
        sql_str = """SELECT * FROM intraday_price WHERE exchange = '%s';""" % market
        data_sql = pd.read_sql_query(sql_str, con=con)
        return data_sql

    @staticmethod
    def get_today_market_data(market, con):
        # sql_str = """SELECT * FROM intraday_price WHERE exchange = '%s' AND WHERE date >= CURDATE();""" % market
        sql_str = """SELECT * FROM intraday_price WHERE date >= CURDATE();"""
        data_sql = pd.read_sql_query(sql_str, con=con)
        data_sql = data_sql[data_sql['exchange'] == market]
        return data_sql
