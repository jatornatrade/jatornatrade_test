import time
from copy import copy
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import subprojects.misc.LowLevelFuncions as llw
from subprojects.types.Tick import Tick
from subprojects.resources.Constants import SymbolsResources
from bs4 import BeautifulSoup
import datetime as dt

logger = llw.script_logger('BMANIA PARSER')


def login_bolsamania(driver):
    logged = False
    attempts = 0
    while attempts < 1 and not logged:
        try:
            url = 'https://www.bolsamania.com/tiemporeal/mercado/MERCADO-CONTINUO'
            user = '********'
            password = '********'
            driver.get(url)
            time.sleep(5)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, "//button[normalize-space()='ACEPTO']"))).click()
            time.sleep(5)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="frm_login"]/input[3]'))).send_keys(user)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="frm_login"]/div[1]/input'))).send_keys(password)
            driver.find_element_by_name('button').click()
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="headerModal"]/div/button'))).click()
            logged = True
            return logged

        except:
            logger.error('Error login in bolsamania')
            logger.info('Retrying...')
            attempts += 1

    return logged


def get_bolsamania_ticks(driver, symbol_data):
    tick_dict = {}
    url = 'https://www.bolsamania.com/tiemporeal/mercado/MERCADO-CONTINUO'
    driver.get(url)
    time.sleep(10)

    names = []
    prices = []
    volumes = []

    attempt = 0

    has_past_data = True
    is_data_ok = False
    while not is_data_ok and has_past_data and attempt < 4:
        html_source = driver.page_source
        soup = BeautifulSoup(html_source, "html.parser")
        names = soup.select('td.text-left.ficha-name')
        prices = soup.select('td.text-right.ficha-price')
        volumes = soup.select('td.text-right.ficha-vol')
        date_times = soup.select('td.text-right.ficha-time')

        i = 0
        for name in names:
            try:
                symbol = SymbolsResources.BOLSAMANIA_SYMBOLS[name.text]
                if symbol == 'BBVA.MC':
                    if len(symbol_data['BBVA.MC']) == 0:
                        logger.error('No past data for BBVA.MC. Impossible to check data')
                        time.sleep(8)
                        html_source = driver.page_source
                        soup = BeautifulSoup(html_source, "html.parser")
                        names = soup.select('td.text-left.ficha-name')
                        prices = soup.select('td.text-right.ficha-price')
                        volumes = soup.select('td.text-right.ficha-vol')
                        date_times = soup.select('td.text-right.ficha-time')
                        has_past_data = False
                        break
                    try:
                        try:
                            volume = int(volumes[i].text.replace(',', ''))
                        except Exception as e:
                            volume = int(volumes[i].text.replace('.', ''))

                        if volume < symbol_data['BBVA.MC']['volume'].values[-1]:
                            logger.warning('Check not passed. Retrying')
                        else:
                            is_data_ok = True
                            logger.log(5, 'Data check pass succesfully')
                            break
                    except Exception as e:
                        logger.warning('Check not passed. Retrying')
                        time.sleep(5)

            except Exception as e:
                logger.warning('Check not passed. Retrying')
                time.sleep(5)

            i += 1
        attempt += 1

    if not is_data_ok:
        logger.warning('Data streamed could has error')

    i = 0
    error_ticks = []
    for name in names:
        try:
            symbol = SymbolsResources.BOLSAMANIA_SYMBOLS[name.text]
            if symbol in SymbolsResources.MC_SYMBOLS:
                try:
                    last_update = dt.datetime.strptime(date_times[i].text, '%d/%m/%y %H:%M')
                except:
                    error_ticks.append(name.text)
                    i += 1
                    continue

                if last_update.date() < dt.datetime.now().date():
                    i += 1
                    continue

                tick = Tick()
                tick_dict[symbol] = copy(tick)
                tick_dict[symbol].name = name.text
                try:
                    tick_dict[symbol].price = float(prices[i].text[:-1].replace(',', '.'))
                except:
                    tick_dict[symbol].price = float(prices[i].text[:-1])

                try:
                    tick_dict[symbol].volume = int(volumes[i].text.replace(',', ''))
                except:
                    tick_dict[symbol].volume = int(volumes[i].text.replace('.', ''))
                tick_dict[symbol].symbol = symbol

        except Exception as e:
            if symbol == 'BBVA.MC':
                logger.warning('Error with BBVA.MC symbol')
                logger.error(e)
            # logger.warning('Error parsing stock : '+symbol)
            error_ticks.append(name.text)
            del tick_dict[symbol]
        i += 1

    if len(error_ticks) > 0:
        error_msg = ', '.join(error_ticks)
        logger.debug('Error parsing stocks: ' + error_msg)

    return tick_dict

