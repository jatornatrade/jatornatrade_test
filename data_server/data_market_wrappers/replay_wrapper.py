from subprojects.resources.Constants import SymbolsResources
from subprojects.types.Tick import Tick
import pandas as pd
import datetime as dt
import copy
import time


def get_replay_ticks():
    tick_dict = {}

    for symbol in SymbolsResources.FX_SYMBOLS:

        time.sleep(3)

        symbol_data = pd.read_csv('testing_and_validation/database_v2/historical/intraday/FX/' + symbol + '.csv', delimiter='\t')

        current_epoch = dt.datetime(dt.datetime.now().year, dt.datetime.now().month, dt.datetime.now().day,
                                    dt.datetime.now().hour, dt.datetime.now().minute)

        data_epoch = symbol_data[symbol_data['date'] == current_epoch.strftime('%Y-%m-%d %H:%M:%S')]

        tick = Tick()
        tick_dict[symbol] = copy.copy(tick)
        tick_dict[symbol].name = symbol
        tick_dict[symbol].price = data_epoch['close'].values[0]
        tick_dict[symbol].volume = 0
        tick_dict[symbol].symbol = symbol

    return tick_dict