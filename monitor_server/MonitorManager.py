import subprojects.misc.LowLevelFuncions as llw
import MySQLdb as mdb
import pandas as pd
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html

logger = llw.script_logger('MONITOR MGR')


class MonitorManager:
    def __init__(self):

        self.trade_sessions = {}

    def read_data_from_mysql(self):

        mysql_connection = mdb.connect('hostname', 'user',
                                            'pwd')

        logger.info('Loading data from database')

        cursor = mysql_connection.cursor()
        databases = "show databases"
        cursor.execute(databases)

        for (databases) in cursor:
            if databases[0].find('metrics') > -1:
                mysql_connection = mdb.connect('hostname', 'user',
                                                    'pwd', databases[0])
                cursor = mysql_connection.cursor()

                cursor.execute("SHOW columns FROM equity")
                fetch = cursor.fetchall()
                columns = []
                for column in fetch:
                    columns.append(column[0])
                cursor.execute('SELECT * FROM equity')
                table_rows = cursor.fetchall()
                equity = pd.DataFrame(table_rows, columns=columns)
                equity.set_index('date', inplace=True)

                cursor.execute("SHOW columns FROM orders")
                fetch = cursor.fetchall()
                columns = []
                for column in fetch:
                    columns.append(column[0])
                cursor.execute('SELECT * FROM orders')
                table_rows = cursor.fetchall()
                orders = pd.DataFrame(table_rows, columns=columns)

                cursor.execute("SHOW columns FROM close_positions")
                fetch = cursor.fetchall()
                columns = []
                for column in fetch:
                    columns.append(column[0])
                cursor.execute('SELECT * FROM close_positions')
                table_rows = cursor.fetchall()
                close_positions = pd.DataFrame(table_rows, columns=columns)

                cursor.execute("SHOW columns FROM open_positions")
                fetch = cursor.fetchall()
                columns = []
                for column in fetch:
                    columns.append(column[0])
                cursor.execute('SELECT * FROM open_positions')
                table_rows = cursor.fetchall()
                open_positions = pd.DataFrame(table_rows, columns=columns)

                del orders['id'], equity['id'], close_positions['id'], open_positions['id']

                data = [equity, orders, close_positions, open_positions]
                self.trade_sessions[databases[0]] = data

        logger.log(5, 'Data loaded correctly')

    def run_monitor(self):

        self.read_data_from_mysql()
        app = dash.Dash('Mi primer gráfico')

        # add sessions here
        app.layout = html.Div([
            dcc.Dropdown(id='my-dropdown',
                         options=[
                             {'label': 'Session1', 'value': 'session01_metrics'},
                             {'label': 'Session2', 'value': 'session02_metrics'},
                             {'label': 'Session3', 'value': 'session03_metrics'},
                             {'label': 'Session4', 'value': 'session04_metrics'},
                             {'label': 'Session5', 'value': 'session05_metrics'}
                         ],
                         value='session01_metrics'
                         ),
            dcc.Graph(id='my-graph')
        ], style={'with': '500'})

        @app.callback(Output('my-graph', 'figure'), [Input('my-dropdown', 'value')])
        def update_graph(selected_dropdown_value):
            self.read_data_from_mysql()
            data = self.trade_sessions[selected_dropdown_value][0]
            return {
                'data': [{
                    'x': data.index,
                    'y': data.total
                }],
                'layout': {'margin': {'1': 40, 'r': 10, 't': 20, 'b': 30}}
            }

        app.run_server('0.0.0.0')
