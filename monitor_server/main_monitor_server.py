import datetime as dt
import sys
import os
import subprojects.misc.LowLevelFuncions as llw
import logging
from monitor_server.MonitorManager import MonitorManager

logger = llw.script_logger('MONITOR SERVER')


class ProgramData:

    def __init__(self, program_name, program_version):
        self.program_name = program_name
        self.program_version = program_version
        self.reference_timestamp = str(dt.datetime.now().strftime("%y%m%d_%H%M%S"))

    def log_header_line(self):
        logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (self.program_name, self.program_version))

    def log_version_line_and_exit(self):
        print('%s (version: %s)' % (self.program_name, self.program_version))
        exit(0)

    def log_man_page_and_exit(self):
        print('##### PROGRAM %s ( VERSION: %s ) #####' % (self.program_name, self.program_version))
        print('Usage: python3 %s [compulsory inputs] {optional inputs}' % sys.argv[0])
        print('Usage: python3 %s [output dir] {optional inputs}' % sys.argv[0].split('/')[-1])
        print()
        print('Allowed options:                                                                     \n'
              '                                                                                     \n'
              'General options:                                                                     \n' 
              '  --help                                     Produce help message                    \n'
              '  --version                                  Output the version                      \n'
              '                                                                                     \n'
              '                                                                                     \n')
        exit(0)

    def detect_standard_inputs(self, argv):

        if '--help' in argv:
            self.log_man_page_and_exit()
        elif '--version' in argv:
            self.log_version_line_and_exit()

    def get_compulsory_inputs(self):
        if len(sys.argv) < 2:
            logger.error('Wrong number of inputs received')
            program_data.log_man_page_and_exit()

        else:
            self.output_path = sys.argv[1]

    def get_optional_inputs(self):
        pass

##################################################
#  INITIAL OPS  ##################################
##################################################


program_data = ProgramData('Monitor Server ', 'v0')

###########################
# INPUT COMPULSORY DATA ###
###########################

program_data.detect_standard_inputs(sys.argv)

program_data.get_compulsory_inputs()

try:
    os.stat(program_data.output_path)
except:
    os.mkdir(program_data.output_path)

logging.basicConfig(filename=program_data.output_path+'/monitor_server.log', filemode='a',
                    format='%(asctime)s [%(levelname)-8s] [%(name)-17s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

logger.info('##### PROGRAM %s ( VERSION: %s ) started #####' % (program_data.program_name, program_data.program_version))

##########################
# INPUT OPTIONAL DATA  ###
##########################

program_data.get_optional_inputs()

monitor_mgr = MonitorManager()

monitor_mgr.run_monitor()

logger.info('### PROGRAM FINISHED ###')
