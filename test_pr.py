import pickle
import unittest
import re
from os import listdir
import subprojects.misc.LowLevelFuncions as llw
from subprojects.parsers.ConfigParserEngine import parse_config as cf_te
from subprojects.parsers.ConfigParserDataServer import parse_config as cf_ds
from monitor_server.MonitorManager import MonitorManager
from trading_engine.SessionManager import SessionManager
from trading_engine.data_handler.FixedDataHandler import FixedDataHandler
from trading_engine.data_handler.ScreenerDataHandler import ScreenerDataHandler
from trading_engine.strategies.Bah import BuyAndHoldStrategy
from trading_engine.strategies.Bol import BollingerStrategy
from trading_engine.strategies.Mac import MovingAverageCrossStrategy
import pandas as pd

import queue
import MySQLdb as mdb

logger = llw.script_logger('TESTING')

class Testing(unittest.TestCase):
    # Global variables
    monitor_manager = MonitorManager()

    def test_configs(self):

        print('')
        logger.info('Config Test')

        files = listdir('data_server/configs')

        for config_file in files:
            if config_file[-4:] != '.ini':
                continue
            cf_ds('data_server/configs/' + config_file)

        files = listdir('trading_engine/configs')

        for config_file in files:
            if config_file[-4:] != '.ini':
                continue
            cf_te('trading_engine/configs/' + config_file)

        self.assertEqual(True, True)

        logger.log(5, 'OK')

    def test_dump_data(self):

        print('')
        logger.info('Dump trade data Test')

        class DummyPortfolio:
            pass

        config = cf_te('testing_and_validation/configs/data_test.ini')
        events = queue.Queue()
        pending_orders = queue.Queue()
        mac_strategy = MovingAverageCrossStrategy(config.symbol_list, events, config, strategy_id='mac')
        bol_strategy = BollingerStrategy(config.symbol_list, events, config, strategy_id='bol')
        bah_strategy = BuyAndHoldStrategy(config.symbol_list, events, config, strategy_id='bah')
        strategy_objects = [mac_strategy, bol_strategy, bah_strategy]
        active_strategies = []
        for strategy in strategy_objects:
            if strategy.status:
                active_strategies.append(strategy)

        portfolio = DummyPortfolio()
        portfolio.symbol_list = config.symbol_list
        output_path = 'output_backtest'
        data_handler = FixedDataHandler(config.symbol_list, config.general.market, config.backtest.frequency,
                                        config.backtest.sampling, config.mysqlconf)

        session_mgr = SessionManager(config, 'backtest', output_path, data_handler, portfolio, active_strategies,
                                     queue.Queue(), pending_orders, config.backtest.start_date)

        self.assertEqual(True, config.general.continue_session)

        logger.log(5, 'OK')

    def test_backtest(self):

        print('')
        logger.info('Backtest Test')

        with open('output_backtest/backtest.dat', "rb") as f:
            pending_orders_list = pickle.load(f)
            screener_deactivated_symbols = pickle.load(f)
            session_data = pickle.load(f)

        total = round(session_data[1][-1]['total'], 3)

        equity_data = pd.read_csv('output_backtest/equity_backtest.txt', skiprows=[1], delim_whitespace=True)
        orders_data = pd.read_csv('output_backtest/orders_backtest.txt', skiprows=[1], delim_whitespace=True)
        positions_data = pd.read_csv('output_backtest/positions_backtest.txt', skiprows=[1], delim_whitespace=True)

        self.assertEqual(17271.489, total)
        self.assertEqual(17271.489, round(equity_data['Total'].values[-1],3))
        self.assertEqual(-14.65360, round(positions_data['Profit'].values[-1],4))
        self.assertEqual(31, len(positions_data))
        self.assertEqual(66, len(orders_data))
        self.assertEqual(1044, len(equity_data))

        logger.log(5, 'OK')

    def test_monitor_server(self):

        print('')
        logger.info('Monitor Server Test')

        self.monitor_manager.read_data_from_mysql()

        con = mdb.connect('hostname', 'user', 'pwd')
        cur = con.cursor()
        insert_stmt = "DROP DATABASE rt_test_metrics"
        cur.execute(insert_stmt)
        con.commit()

        result = True
        self.assertEqual(True, result)

        logger.log(5, 'OK')

    def test_realtime(self):

        print('')
        logger.info('Realtime Test')

        with open('output_rt/rt_test.dat', "rb") as f:
            pending_orders_list = pickle.load(f)
            screener_deactivated_symbols = pickle.load(f)
            session_data = pickle.load(f)

        total = round(session_data[1][-1]['total'], 3)

        equity_data = pd.read_csv('output_rt/equity_rt_test.txt', skiprows=[1], delim_whitespace=True)
        orders_data = pd.read_csv('output_rt/orders_rt_test.txt', skiprows=[1], delim_whitespace=True)
        positions_data = pd.read_csv('output_rt/positions_rt_test.txt', skiprows=[1], delim_whitespace=True)
        cur_pos_data = pd.read_csv('output_rt/curpositi_rt_test.txt', skiprows=[1], delim_whitespace=True)

        self.assertEqual(0, len(positions_data))
        self.assertEqual(8, len(orders_data))
        self.assertEqual(9934.246, total)
        self.assertEqual(9934.246, equity_data['Total'].values[-1])
        self.assertEqual(8, len(self.monitor_manager.trade_sessions['rt_test_metrics'][3]))
        self.assertEqual(-8.2302, pd.to_numeric(self.monitor_manager.trade_sessions['rt_test_metrics'][3]['profit'].
                                                values[0]))
        self.assertEqual(-8.2395, pd.to_numeric(self.monitor_manager.trade_sessions['rt_test_metrics'][3]['profit'].
                                                values[-1]))
        self.assertEqual(-8.2302, cur_pos_data['Profit'].values[0])
        self.assertEqual(-8.2395, cur_pos_data['Profit'].values[-1])

        logger.log(5, 'OK')

    def test_data_server(self):

        print('')
        logger.info('Data Server Test')

        f = open("output_ds/hist_test.log", "r")
        pattern1 = "Download 2 out of 2 symbols"
        pattern2 = "ERROR"

        result = False
        for line in f:
            if re.search(pattern1, line):
                result = True
            if re.search(pattern2, line):
                result = False

        f.close()

        self.assertEqual(True, result)

        logger.log(5, 'OK')


    def test_post_processor(self):

        print('')
        logger.info('Post-Processor Test')

        f = open("output_backtest/postprocessor.log", "r")
        pattern1 = "ERROR"

        result = True
        for line in f:
            if re.search(pattern1, line):
                result = False

        f.close()

        self.assertEqual(True, result)

        logger.log(5, 'OK')


if __name__ == '__main__':

    logger.info('Launching Pull Request Test')

    unittest.main()
