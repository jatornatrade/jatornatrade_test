from trading_engine.Event import SignalEvent
from trading_engine.strategies.Strategy import Strategy
from trading_engine.Event import EventType
from subprojects.types.Common import *
import datetime
import subprojects.misc.LowLevelFuncions as llw
from subprojects.algorithm.TechnicalIndicators import *

logger = llw.script_logger('CAR STRATEGY')


class CarvasBoxStrategy(Strategy):
    """
    CarvasBoxStrategy Strategy
    """

    def __init__(self, symbol_list, deactivated_symbols, events, config, strategy_id):

        self.status = False
        self.symbol_list = symbol_list
        self.deactivated_symbols = deactivated_symbols
        self.events = events
        self.limit_orders = config.car_strategy.limit_orders
        self.stop_loss = config.car_strategy.stop_loss / 100
        self.take_profit = config.car_strategy.take_profit / 100 + 1
        self.trailing_stop = config.car_strategy.trailing_stop
        self.enable_debug_file = config.car_strategy.enable_debug_file
        self.output_path = config.general.output_path
        self.strategy_id = strategy_id
        self.signal_number = 0
        self.bought = {}
        self.periods_in_market = {}
        self.periods_in_watchlist = {}
        self.periods_in_consolidation = {}

        self._update_bought()
        self._check_strategy_status(config.strategies)

        if self.enable_debug_file:
            self._print_debug_header()

    def _update_bought(self):

        for s in self.symbol_list:
            if s not in self.bought:
                self.bought[s] = PositionStatus.OUT
            if s not in self.periods_in_market:
                self.periods_in_market[s] = 0
            if s not in self.periods_in_watchlist:
                self.periods_in_watchlist[s] = 0
            if s not in self.periods_in_consolidation:
                # item 1 days in period, item 2 init price consolidate
                self.periods_in_consolidation[s] = [0, 0]

    def _check_strategy_status(self, strategy_list):
        if self.strategy_id in strategy_list:
            self.status = True

    def calculate_signals(self, event, data_handler, current_date, orders_data, ppv):

        if self.status:

            self._update_bought()

            if event.type == EventType.MARKET:
                for symbol in self.symbol_list:

                    self.periods_in_market[symbol] = (self.periods_in_market[symbol] + 1) if self.bought[
                                                                                                 symbol] == PositionStatus.LONG else 0

                    self.periods_in_watchlist[symbol] = (self.periods_in_watchlist[symbol] + 1) if self.bought[
                                                                                                       symbol] == \
                                                                                                   PositionStatus.OUT else 0

                    if not data_handler.has_current_price(symbol, current_date):
                        continue

                    bars = data_handler.get_current_data(symbol, current_date - datetime.timedelta(15),
                                                         current_date)

                    if len(bars) < 10:
                        continue

                    avg_volume = np.mean(bars.volume.values[-10:])

                    volume = data_handler.get_current_tick(symbol, current_date, 'volume')
                    price = data_handler.get_current_tick(symbol, current_date, 'close')

                    volume_change = (volume / bars.volume.values[-2] - 1) * 100

                    if price <= 0:
                        continue

                    is_consolided = True if volume < avg_volume * 0.3 else False
                    if is_consolided:
                        if self.periods_in_consolidation[symbol][0] == 0:
                            self.periods_in_consolidation[symbol][1] = price

                        if price > self.periods_in_consolidation[symbol][1] * 1.08 or price < \
                                self.periods_in_consolidation[symbol][1] * 0.92:
                            is_consolided = False

                    if not is_consolided:
                        self.periods_in_consolidation[symbol][1] = 0

                    self.periods_in_consolidation[symbol][0] = (self.periods_in_consolidation[symbol][0] + 1) if \
                        is_consolided and self.bought[
                            symbol] == \
                        PositionStatus.OUT else 0

                    strength = 1.0
                    signal_detected = False

                    if symbol not in self.deactivated_symbols.queue and \
                            self.bought[symbol] == PositionStatus.OUT and self.periods_in_consolidation[symbol][0] > 4\
                            and volume_change > 30:
                        self.periods_in_watchlist[symbol] = 0
                        self.periods_in_consolidation[symbol][0] = 0
                        self.periods_in_consolidation[symbol][1] = 0

                        self.signal_number += 1
                        signal_detected = True
                        action = OrderType.LONG
                        signal_id = self.strategy_id + '{:04d}'.format(self.signal_number)

                    if self.bought[symbol] == PositionStatus.LONG and self.periods_in_market[symbol] >= 3:
                        signal_detected = True
                        action = OrderType.EXIT
                        orders_data_tmp = orders_data[orders_data['Symbol'] == symbol]
                        orders_data_tmp = orders_data_tmp[orders_data_tmp['Direction'] == OrderDirection.BUY.value]
                        orders_data_tmp = orders_data_tmp[
                            orders_data_tmp['Order_ID'].str.contains(self.strategy_id)]
                        signal_id = orders_data_tmp['Order_ID'].values[-1]

                    if signal_detected:
                        signal = SignalEvent(signal_id, SignalType.MKT, symbol, current_date, action, strength)
                        self.events.put(signal)
                        logger.debug('Signal detected! ' + current_date.strftime('%Y/%m/%d %H:%M:%S'))
                        logger.debug(action.value + ' ' + symbol + ' : ' + str(price))
                        logger.debug('ID: ' + signal_id)

                    if symbol not in self.deactivated_symbols.queue and self.bought[symbol] != PositionStatus.LONG and \
                            self.periods_in_watchlist[symbol] > 4 and \
                            self.periods_in_consolidation[symbol][0] == 0:
                        # reset dictionaries because if symbol appears again in screener
                        self.periods_in_market[symbol] = 0
                        self.periods_in_watchlist[symbol] = 0
                        self.periods_in_consolidation[symbol][0] = 0
                        self.periods_in_consolidation[symbol][1] = 0

                        self.deactivated_symbols.put(symbol)

    def _print_debug_header(self):

        if self.enable_debug_file:
            header = " Symbol        ID  Action        Date  Time  Quantity      OPrice  OCommission       Cost       CDate CTime      CPrice  CCommission     Paying       Profit    Profit_%\n" \
                     "-------   -------  ------  ---------- -----  --------  ----------  -----------  ---------  ---------- -----  ----------  -----------  ---------  -----------  ----------\n"

            f = open(self.output_path + '/car_debug.txt', 'w')
            f.write(header)
            f.close()
