from subprojects.types.Common import *
import subprojects.misc.LowLevelFuncions as llw
import sys
from pandas_datareader import data as pdr
from fhub import Session
import datetime as dt

logger = llw.script_logger('STOCK DATA WRAPPER')


def download_from_pdr(symbol, frequency, market):

    if market == Market.FX:
        symbol = symbol + '=X'

    if frequency == Frequency.DAILY:

        data = pdr.get_data_yahoo(symbol, start='01-01-2000', end=dt.date.today().strftime("%d/%m/%Y"))
        data.index = data.index.normalize()
        data.index = data.index.tz_localize(None)
        data.index.names = ['date']
        adjust_factor = (data['Adj Close'] / data['Close']).values
        data['Close'] = data['Adj Close']
        del data['Adj Close']
        data.columns = ['high', 'low', 'open', 'close', 'volume']
        data = data.sort_values(['date'], ascending=[True])
        data = data[['open', 'high', 'low', 'close', 'volume']]
        data['open'] = data['open'] * adjust_factor
        data['high'] = data['high'] * adjust_factor
        data['low'] = data['low'] * adjust_factor
        if market != Market.FX:
            data = data[data['volume'] > 0]
        data = data.sort_values(['date'], ascending=[True])

        return data
    else:
        logger.error('No intraday data source implement for yahoo')
        logger.info('Program aborted')
        sys.exit()


def download_from_fhub(symbol, frequency, market):

    hub = Session('finnhubkey')
    if market == Market.FX:
        symbol = symbol + '=X'

    if frequency == Frequency.DAILY:

        data = hub.candle(symbol, end=(dt.datetime.today() + dt.timedelta(days=1)).strftime(format='%Y-%m-%d'))
        data.index = data.index.normalize()
        data.index = data.index.tz_localize(None)
        data.index.names = ['date']
        data = data[['open', 'high', 'low', 'close', 'volume']]
        if market != Market.FX:
            data = data[data['volume'] > 0]
        data = data.sort_values(['date'], ascending=[True])

        return data