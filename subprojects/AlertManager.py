import requests
from subprojects.types.Common import *


class AlertManager:

    def __init__(self, config, is_data_server):
        self.config = config

        self.is_data_server = is_data_server

        if config.general.alerts_bot == 'bot_name':
            self.id_group = "idgroup"
            self.token = "token"

        elif config.general.alerts_bot == 'bot_name':
            self.id_group = "idgroup"
            self.token = "token"

    def send_telegram_alert(self, msg):

        if not self.is_data_server:
            if self.config.general.mode == EngineMode.REALTIME and self.config.general.tgm_alerts:
                self.send_msg(msg)

        if self.is_data_server and self.config.general.tgm_alerts:
            self.send_msg(msg)

    def send_msg(self, msg):
        id = self.id_group
        token = self.token

        url = "https://api.telegram.org/bot" + token + "/sendMessage"
        params = {
            'chat_id': id,
            'text': msg
        }
        requests.post(url, params=params)
