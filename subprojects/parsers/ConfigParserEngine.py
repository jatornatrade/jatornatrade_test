import configparser
import sys
import datetime as dt
import subprojects.misc.LowLevelFuncions as llf
from subprojects.types.Common import *
from subprojects.resources.Constants import SymbolsResources
import traceback

logger = llf.script_logger('TE CONFIG PARSER')

#################
# CONFIG PARSER #
#################


def parse_config(config_path):

    config_file = configparser.ConfigParser()
    config_file.read(config_path)

    config = Config()

    try:
        config.general.mode = EngineMode(config_file['GENERAL']['Mode'].upper())
        config.general.market = Market(config_file['GENERAL']['Market'].upper())
        config.general.continue_session = config_file.getboolean('GENERAL', 'ContinueSession')
        config.general.tgm_alerts = config_file.getboolean('GENERAL', 'TelegramAlerts')
        config.general.alerts_bot = config_file['GENERAL']['AlertsBot']
        config.general.monitoring = config_file.getboolean('GENERAL', 'Monitoring')

        config.data_handler.type = DataHandlerType(config_file['DATA_HANDLER']['Type'].upper())
        config.data_handler.screener_db_path = config_file.get('DATA_HANDLER', 'ScreenerDB')

        config.mysqlconf.db_host = config_file['MYSQL']['Hostname']
        config.mysqlconf.db_port = config_file.getint('MYSQL', 'Port')
        config.mysqlconf.db_user = config_file['MYSQL']['User']
        config.mysqlconf.db_pass = config_file['MYSQL']['Password']
        config.mysqlconf.db_name = config_file['MYSQL']['DbName']

        config.backtest.start_date = dt.datetime.strptime(config_file['BACKTEST']['StartDate'] + ' ' +
                                                          config_file['BACKTEST']['StartTime'], '%d/%m/%Y %H:%M:%S')
        config.backtest.end_date = dt.datetime.strptime(config_file['BACKTEST']['EndDate'] + ' ' +
                                                        config_file['BACKTEST']['EndTime'], '%d/%m/%Y %H:%M:%S')
        config.backtest.frequency = Frequency(config_file['BACKTEST']['Frequency'].upper())
        config.backtest.sampling = config_file.getint('BACKTEST', 'MinuteSampling')

        config.realtime.mode = RTMode(config_file['REALTIME']['Mode'].upper())
        config.realtime.frequency = Frequency(config_file['REALTIME']['Frequency'].upper())
        config.realtime.sampling = config_file.getint('REALTIME', 'MinuteSampling')
        config.realtime.daily_exec_time = dt.time.fromisoformat(config_file['REALTIME']['DailyExecTime'])
        config.realtime.hostname = config_file['REALTIME']['DataHostname']
        config.realtime.port = config_file.getint('REALTIME', 'DataPort')

        config.portfolio.initial_capital = config_file.getfloat('PORTFOLIO', 'InitialCapital')
        config.portfolio.commiss_model = ComissModel(config_file['PORTFOLIO']['CommissModel'].upper())
        config.portfolio.commission = config_file.getfloat('PORTFOLIO', 'Commission')
        config.portfolio.max_commission = config_file.getfloat('PORTFOLIO', 'MaxCommission')
        config.portfolio.min_commission = config_file.getfloat('PORTFOLIO', 'MinCommission')
        config.portfolio.spread = config_file.getfloat('PORTFOLIO', 'Spread')
        config.portfolio.slippage = config_file.getfloat('PORTFOLIO', 'Slippage')
        config.portfolio.swap = config_file.getfloat('PORTFOLIO', 'Swap')
        config.portfolio.signal_rate = config_file.getint('PORTFOLIO', 'SignalRate')

        config.ppv.interpolate = config_file.getboolean('PPV', 'Interpolate')
        config.ppv.smoothing = config_file.getboolean('PPV', 'Smoothing')

        config.risk_manager.order_invest = config_file.getfloat('RISK_MANAGER', 'OrderInvest')
        config.risk_manager.limit_orders = config_file.getboolean('RISK_MANAGER', 'LimitOrders')
        config.risk_manager.comm_in_limits = config_file.getboolean('RISK_MANAGER', 'CommInLimits')
        config.risk_manager.order_invest = config_file.getfloat('RISK_MANAGER', 'OrderInvest')

        config.mac_strategy.long_window = config_file.getint('MAC', 'LongWindow')
        config.mac_strategy.short_window = config_file.getint('MAC', 'ShortWindow')
        config.mac_strategy.long_window = config_file.getint('MAC', 'LongWindow')
        config.mac_strategy.limit_orders = config_file.getboolean('MAC', 'LimitOrders')
        config.mac_strategy.stop_loss = config_file.getfloat('MAC', 'StopLoss')
        config.mac_strategy.take_profit = config_file.getfloat('MAC', 'TakeProfit')
        config.mac_strategy.trailing_stop = config_file.getboolean('MAC', 'TrailingStop')
        config.car_strategy.enable_debug_file = config_file.getboolean('MAC', 'DebugFile')

        config.bol_strategy.allowed_signals = AllowedSignals(config_file['BOL']['AllowedSignals'].upper())
        config.bol_strategy.window = config_file.getint('BOL', 'Window')
        config.bol_strategy.n_std = config_file.getint('BOL', 'NStd')
        config.bol_strategy.limit_orders = config_file.getboolean('BOL','LimitOrders')
        config.bol_strategy.stop_loss = config_file.getfloat('BOL', 'StopLoss')
        config.bol_strategy.take_profit = config_file.getfloat('BOL', 'TakeProfit')
        config.bol_strategy.trailing_stop = config_file.getboolean('BOL','TrailingStop')
        config.car_strategy.enable_debug_file = config_file.getboolean('BOL', 'DebugFile')

        config.car_strategy.limit_orders = config_file.getboolean('CAR', 'LimitOrders')
        config.car_strategy.stop_loss = config_file.getfloat('CAR', 'StopLoss')
        config.car_strategy.take_profit = config_file.getfloat('CAR', 'TakeProfit')
        config.car_strategy.trailing_stop = config_file.getboolean('CAR', 'TrailingStop')
        config.car_strategy.enable_debug_file = config_file.getboolean('CAR', 'DebugFile')

    except Exception as e:
        logger.error('Error parsing config')
        logger.error('Program interrumped')
        logger.error(traceback.format_exc())
        sys.exit()


    # TODO: Save these information in a dictionary
    if config.general.market == Market('MC'):
        market_symbol_list = SymbolsResources.MC_SYMBOLS
    elif config.general.market == Market('DE'):
        market_symbol_list = SymbolsResources.DE_SYMBOLS
    elif config.general.market == Market('FX'):
        market_symbol_list = SymbolsResources.FX_SYMBOLS
    elif config.general.market == Market('CC'):
        market_symbol_list = SymbolsResources.CC_SYMBOLS
    elif config.general.market == Market('US'):
        market_symbol_list = SymbolsResources.US_SYMBOLS

    # TODO: Improve better data check
    for symbol in config_file.items('SYMBOLS'):
        if symbol[1] == 'true' and symbol[0].upper() in market_symbol_list:
            config.symbol_list.append(symbol[0].upper())
        elif symbol[1] == 'true' and symbol[0].upper() not in market_symbol_list:
            logger.warning(symbol[0] + ' not in ' + config.general.market.value + ' market. Symbol discarted.')

    for strategy in config_file.items('STRATEGIES'):
        if config_file.getboolean('STRATEGIES', strategy[0]):
            config.strategies.append(strategy[0])

    for output_file in config_file.items('CORE'):
        if config_file.getboolean('CORE', output_file[0]):
            config.core.append(output_file[0])

    logger.log(5, 'Config parsed correctly')

    return config


class ConfigGeneral:
    mode = EngineMode
    market = Market
    continue_session = False
    tgm_alerts = False
    alerts_bot = ''
    monitoring = False
    output_path = None


class ConfigDataHandler:
    type = DataHandlerType
    screener_db_path = ''


class ConfigMysql:
    db_host = ''
    db_port = 0
    db_user = ''
    db_pass = ''
    db_name = ''


class ConfigBacktest:
    start_date = ''
    end_date = ''
    frequency = Frequency
    sampling = 0


class ConfigPortfolio:
    initial_capital = 0
    commiss_model = ComissModel
    commission = 0
    max_commission = 0
    min_commission = 0
    spread = 0
    slippage = 0
    swap = 0
    signal_rate = 100


class ConfigPPV:
    interpolate = False
    smoothing = False


class ConfigRiskManager:
    limit_orders = False
    comm_in_limits = False
    order_invest = 0


class ConfigRealtime:
    frequency = Frequency
    sampling = 0
    daily_exec_time = ''
    hostname = ''
    port = 0


class ConfigMAC:
    allowed_signals = AllowedSignals('ALL')
    long_window = 0
    short_window = 0
    limit_orders = False
    stop_loss = 0
    take_profit = 0
    trailing_stop = False
    enable_debug_file = False


class ConfigBOL:
    allowed_signals = AllowedSignals('ALL')
    window = 0
    n_std = 0
    limit_orders = False
    stop_loss = 0
    take_profit = 0
    trailing_stop = False
    enable_debug_file = False


class ConfigCAR:
    limit_orders = False
    stop_loss = 0
    take_profit = 0
    trailing_stop = False
    enable_debug_file = False


class Config:
    general = ConfigGeneral()
    data_handler = ConfigDataHandler()
    mysqlconf = ConfigMysql()
    backtest = ConfigBacktest()
    realtime = ConfigRealtime()
    portfolio = ConfigPortfolio()
    ppv = ConfigPPV()
    risk_manager = ConfigRiskManager()
    mac_strategy = ConfigMAC()
    bol_strategy = ConfigBOL()
    car_strategy = ConfigCAR()
    core = []
    strategies = []
    symbol_list = []
