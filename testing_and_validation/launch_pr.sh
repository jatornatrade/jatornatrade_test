#!/bin/bash

export PYTHONPATH=.
timeout 180 faketime '2020-10-06 16:00:30' python3 trading_engine/main_trading_engine.py testing_and_validation/configs/rt_test.ini rt_test output_rt &
timeout 180 faketime '2020-10-06 16:00:30' python3 data_server/main_data_server.py testing_and_validation/configs/ds_rt_test.ini ds_test output_dsrt
python3 trading_engine/main_trading_engine.py testing_and_validation/configs/backtest_test.ini backtest output_backtest
python3 data_server/main_data_server.py testing_and_validation/configs/ds_hist_test.ini hist_test output_ds
python3 post_processor/main_post_processor.py output_backtest